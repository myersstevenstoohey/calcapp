
var bigScreen = document.querySelector("#log");
var bigScreenBack = document.querySelector("#view_log_back");
var topScreen = document.querySelector("#screen");
var totalScreen = document.querySelector("#total");
var total = 0;
var lastNum = "";
var decimalFlag = false;
var minusFlag = false;

document.onkeyup = function(e) {
    // console.log('i stroke a key', e);
    // document.querySelector('#screen').innerHTML = e.key;
    // if (e.key == 'Enter') {
    //     //take current and add previous number
    //     lastNum = greenScreen.innerHTML;
    //     total += lastNum;
    //     totalScreen.innerHTML = total;
    //     //show on log screen
    //     addLog(lastNum);
    //     //clear screen
    //     showScreen("");
    //     lastNum = "";
    // } else if (e.key == "+") {
    //     //
    // } else if (e.key == "-"){
    //     //clear key
    // } else {
    //     lastNum += e.key;
    //     showScreen(lastNum);
    // }
    console.log(e.key);
    var keyStorage = e.key;
    switch(keyStorage){
        case "0":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "1":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "2":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "3":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "4":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "5":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "6":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "7":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "8":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "9":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "Enter":
            if (lastNum && !isNaN(lastNum)) {
                lastNum = parseFloat(topScreen.innerHTML);
                total += parseFloat(lastNum);
                totalScreen.innerHTML = total.toLocaleString();
                if (!minusFlag) {
                    addLog(lastNum.toLocaleString() + " +");
                } else {
                    addLog(lastNum.toLocaleString());
                }
                showScreen("");
                lastNum = "";
                bigScreenBack.scrollTop = bigScreenBack.scrollHeight;
                decimalFlag = false;
                minusFlag = false;
            }
            break;
        case "-":
            //do minus
            if(!minusFlag){
                minusFlag = true;
                lastNum += e.key;
                showScreen(lastNum);
            }
            //possible function to fetch previous number
            break;
        case "Backspace":
            //do something
            lastNum = lastNum.slice(0,-1);
            showScreen(lastNum);
            break;
        case ".":
            //do something
            if (!decimalFlag){
                //if flag variable is false, add decimal
                decimalFlag = true;
                lastNum += e.key;
                showScreen(lastNum);
            }
            break;
        default:
            //do nothing
            break;

    }
};


var showScreen = function(someValue){
    document.querySelector('#screen').innerHTML = someValue;
};

var addLog = function(theKey){
    var log = document.createElement("li");
    var input = document.createTextNode(theKey);
    if(minusFlag){
        log.className = "red";
    }
    log.appendChild(input);
    bigScreen.appendChild(log);
};

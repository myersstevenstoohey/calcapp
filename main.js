
// console.log("testing...");
const{app, BrowserWindow,Menu} = require('electron');

//Setting Environment
// process.env.NODE_ENV = 'production';

// main process
const path = require('path');
const url = require('url');
require('electron-reload')(__dirname);
let win;

function createWindow(){
    win = new BrowserWindow({width:450, height:700});
    win.loadURL(url.format({
        pathname:path.join(__dirname, 'mainWindow.html'),
        protocol: 'file',
        slashes:true
    }));

    const template = [
        {
            label: 'File',
            submenu: [
                {label:'Export'},
                {role: 'reload'},
                {role:'close'}
            ]
        }
        // ,
        // {
        //     label: 'View',
        //     submenu: [
        //         {role: 'reload'},
        //         {role: 'forcereload'},
        //         {role: 'toggledevtools'},
        //         {type: 'separator'},
        //         {role: 'resetzoom'},
        //         {role: 'zoomin'},
        //         {role: 'zoomout'},
        //         {type: 'separator'},
        //         {role: 'togglefullscreen'}
        //     ]
        // },
        // {
        //     role: 'window',
        //     submenu: [
        //         {role: 'minimize'},
        //         {role: 'close'}
        //     ]
        // },
        // {
        //     role: 'help',
        //     submenu: [
        //         {
        //             label: 'Learn More',
        //             click () { require('electron').shell.openExternal('https://electron.atom.io') }
        //         }
        //     ]
        // }
    ];

    if (process.platform === 'darwin') {
        template.unshift({
            label: app.getName(),
            submenu: [
                {role: 'about'},
                {type: 'separator'},
                {role: 'services', submenu: []},
                {type: 'separator'},
                {role: 'hide'},
                {role: 'hideothers'},
                {role: 'unhide'},
                {type: 'separator'},
                {role: 'quit'}
            ]
        });

        // Edit menu
        // template[1].submenu.push(
        //     {type: 'separator'},
        //     {
        //         label: 'Speech',
        //         submenu: [
        //             {role: 'startspeaking'},
        //             {role: 'stopspeaking'}
        //         ]
        //     }
        // )

        // Window menu
    //     template[3].submenu = [
    //         {role: 'close'},
    //         {role: 'minimize'},
    //         {role: 'zoom'},
    //         {type: 'separator'},
    //         {role: 'front'}
    //     ]
    }

    if(process.env.NODE_ENV !=='production'){
        template.push({
            label:'Developer Tools',
            submenu:[
                {
                    label:'Toggle DevTools',
                    accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                    click(item, focusedWindow){
                        focusedWindow.toggleDevTools();
                    }
                }
            ]
        })
    }

    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);

    win.on('closed', function(){
        win = null
    });
    //open dev tools;
    // win.openDevTools()
}


app.on('ready', function(){
    createWindow();
});
app.on('window-all-closed', function(){
    app.quit();
});

app.on("activate", function(){
    if (win == null){
    createWindow();
}
});

